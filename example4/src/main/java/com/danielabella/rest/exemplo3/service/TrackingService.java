package com.danielabella.rest.exemplo3.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.danielabella.rest.exemplo3.domain.Tracking;
import com.danielabella.rest.exemplo3.repository.TrackingRepository;

/**
* 
* @author Mateus
*
*
*/

@Service
public class TrackingService {
	
	@Autowired
	private TrackingRepository trackingRepository;
	//get
	public  List<Tracking>ListarRastreamentos() {
		return trackingRepository.findAll();
		
	}
	//get
	public Tracking obterTrackingPorCodigo(int codigo_tr) {
		return trackingRepository.findOne(codigo_tr);
	}
	
	//post implementar
	
	public Tracking criarTracking(Tracking tracking) {
		return trackingRepository.save(tracking);
	}

	//put 
	
	public void atualizarTracking(Tracking tracking) {
		trackingRepository.save(tracking);
	}
	
	//deletar
	
	public void deletarTracking(int id_track) {
		trackingRepository.delete(id_track);
	}

}
