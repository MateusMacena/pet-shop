package com.danielabella.rest.exemplo3.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import com.danielabella.rest.exemplo3.domain.Entrega;

@RepositoryRestResource (collectionResourceRel = "entrega", path = "entrega")
public interface EntregaRepository extends JpaRepository<Entrega, Integer>{



}
